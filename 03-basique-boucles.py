from jinja2 import Environment, FileSystemLoader

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/03-basique-boucles.j2')

# 2- ajouter les variables à substituer
liste = ["alain", "alice", "jean", "bernard"]

# 3- rendering (= génération du résultat)
output = template.render(utilisateurs=liste)
# imprimer résultat
print(output)
# écrire résultat dans un fichier .txt
f = open("./results/03-basique-boucles.txt", "w")
f.write(output)
f.close()
