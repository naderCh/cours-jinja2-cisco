import yaml
from jinja2 import Environment, FileSystemLoader

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/08-config_router_yml.j2')

# 2- ajouter les variables à substituer depuis .yml
with open('./datafiles/08-config_router_yml.yml', 'r') as f:
    reader = yaml.safe_load(f)  # reader est un dictionnaire

# 3- rendering (= génération du résultat)
output = template.render(**reader)
# imprimer résultat
print(output)
# écrire résultat dans un fichier
f = open("results/08-config_router_yml.txt", "w")
f.write(output)
f.close()

'''
#optionel: se connecter sur un router et exécuter la config générée
#nécessite d'installer netmiko: exécutez "pip install netmiko" sur une invite de commande

import netmiko

connexion_router = ConnectHandler(ip="192.168.1.200",device_type="cisco_ios",username="cisco",password="cisco")
liste_commandes = output.split("\n") # le output sera divisé en liste de commande à exécuter une après l'autre!!
res = connexion_router.send_config_set(liste_commandes) #exécuter les commandes sur le router
print(res)
connexion_router.disconnect() #déconnecter du switch
'''
