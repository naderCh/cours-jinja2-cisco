from jinja2 import Environment, FileSystemLoader

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/05-config_init.j2')

# 2- ajouter les variables à substituer
msg_login = "acces interdit"
msg_motd = "bonjour"
hostname = "SW1"
passw_priv = "class"
passw_console = "cisco"
passw_vty = "cisco"

# 3- rendering (= génération du résultat)
output = template.render(msg_login=msg_login, msg_motd=msg_motd,
                         hostname=hostname, passw_priv=passw_priv,
                         passw_console=passw_console, passw_vty=passw_vty)
# imprimer résultat
print(output)
# écrire résultat dans un fichier
f = open("./results/05-config_init.txt", "w")
f.write(output)
f.close()
