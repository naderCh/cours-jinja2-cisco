from jinja2 import Environment, FileSystemLoader
import yaml

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/04-basique-boucles-et-yml.j2')

# 2- ajouter les variables à substituer
with open('./datafiles/04-basique-boucles-et-yml.yml', 'r') as f:
    reader = yaml.safe_load(f)  # reader est un dictionnaire

# 3- rendering (= génération du résultat)
output = template.render(**reader)
# imprimer résultat
print(output)
# écrire résultat dans un fichier .txt
f = open("./results/04-basique-boucles-et-yml.txt", "w")
f.write(output)
f.close()
