from jinja2 import Environment, FileSystemLoader

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/07-config_vlan_int.j2')

# 2- ajouter les variables à substituer
fe01 = ('f 0/1', 'vlan 10')
fe02 = ('f 0/2', 'vlan 11')
fe03 = ('f 0/3', 'vlan 12')
fe04 = ('f 0/4', 'vlan 10')
fe05 = ('f 0/5', 'vlan 11')
fe06 = ('f 0/6', 'vlan 12')
fe07 = ('f 0/7', 'vlan 10')
fe08 = ('f 0/8', 'vlan 11')
fe09 = ('f 0/9', 'vlan 12')

interfaces_access = [fe01, fe02, fe03, fe04, fe05, fe06, fe07, fe08, fe09]
# rendering
output = template.render(interfaces_access=interfaces_access)
# imprimer résultat
print(output)
# écrire résultat dans un fichier
f = open("./results/07-config_vlan_int.txt", "w")
f.write(output)
f.close()

'''
#optionel: se connecter sur un switch et exécuter la config générée
#nécessite d'installer netmiko: exécutez "pip install netmiko" sur une invite de commande

import netmiko

connexion_switch = ConnectHandler(ip="192.168.1.200",device_type="cisco_ios",username="cisco",password="cisco")
liste_commandes = output.split("\n") # le output sera divisé en liste de commande à exécuter une après l'autre!!
res = connexion_switch.send_config_set(liste_commandes) #exécuter les commandes sur le switch
print(res)
connexion_switch.disconnect() #déconnecter du switch
'''
