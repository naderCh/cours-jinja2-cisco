import csv
from jinja2 import Environment, FileSystemLoader

source_file = "datafiles/09-switch-ports.csv"
# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/09-switch-ports.j2')
output = ""

# 2- ajouter les variables à substituer depuis csv
r = []
with open(source_file) as f:
    reader = csv.DictReader(f, delimiter=';')
    r = list(reader)  # convertir reader en liste

# 3- rendering (= génération du résultat)
output = template.render(interfaces=r)
# imprimer résultat
print(output)
# écrire résultat dans un fichier
f = open("results/09-switch-ports.txt", "w")
f.write(output)
f.close()

'''
#optionel: se connecter sur un switch et exécuter la config générée
#nécessite d'installer netmiko: exécutez "pip install netmiko" sur une invite de commande

import netmiko

connexion_switch = ConnectHandler(ip="192.168.1.200",device_type="cisco_ios",username="cisco",password="cisco")
liste_commandes = output.split("\n") # le output sera divisé en liste de commande à exécuter une après l'autre!!
res = connexion_switch.send_config_set(liste_commandes) #exécuter les commandes sur le switch
print(res)
connexion_switch.disconnect() #déconnecter du switch
'''
