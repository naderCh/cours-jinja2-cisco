from jinja2 import Environment, FileSystemLoader
from datetime import date
# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/01-basique-variables.j2')

# 2- ajouter les variables à substituer
u = "user1"
machine = "pc1"
aujourdhui = date.today()

# 3- rendering (= génération du résultat)
output = template.render(util=u, nom=machine, date=aujourdhui)
# imprimer résultat
print(output)
# écrire résultat dans un fichier .txt
f = open("./results/01-basique-resultat.txt", "w")
f.write(output)
f.close()
