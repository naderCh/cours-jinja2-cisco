from jinja2 import Environment, FileSystemLoader
from datetime import datetime

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/02-basique-conditions.j2')

# 2- ajouter les variables à substituer
heure = datetime.now().strftime("%H")

# 3- rendering (= génération du résultat)
output = template.render(heure_du_jour=heure)
# imprimer résultat
print(output)
# écrire résultat dans un fichier .txt
f = open("./results/02-basique-conditions.txt", "w")
f.write(output)
f.close()
