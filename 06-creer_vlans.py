from jinja2 import Environment, FileSystemLoader

# 1- charger le fichier .j2
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template('./templates/06-config_vlans.j2')

# 2- ajouter les variables à substituer
vlan10 = ('vlan10', 'machines-clients')
vlan11 = ('vlan11', 'serveurs')
vlan12 = ('vlan12', 'imprimantes')
liste = [vlan10, vlan11, vlan12]

# 3- rendering (= génération du résultat)
output = template.render(vlans=liste)
# imprimer résultat
print(output)
# écrire résultat dans un fichier
f = open("./results/06-config_vlans.txt", "w")
f.write(output)
f.close()
